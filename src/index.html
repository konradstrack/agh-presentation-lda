<!doctype html>
<html lang="en">

	<head>
		<meta charset="utf-8">

		<title>Latent Dirichlet Allocation</title>

		<meta name="description" content="Latent Dirichlet Allocation and Conditional Random Fields">
		<meta name="author" content="Martyna Wałaszewska, Konrad Strack">

		<meta name="apple-mobile-web-app-capable" content="yes" />
		<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />

		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<link rel="stylesheet" href="css/reveal.min.css">
		<link rel="stylesheet" href="css/theme/custom.css" id="theme">

		<!-- For syntax highlighting -->
		<link rel="stylesheet" href="lib/css/zenburn.css">

		<!-- If the query includes 'print-pdf', use the PDF print sheet -->
		<script>
			document.write( '<link rel="stylesheet" href="css/print/' + ( window.location.search.match( /print-pdf/gi ) ? 'pdf' : 'paper' ) + '.css" type="text/css" media="print">' );
		</script>

		<!--[if lt IE 9]>
		<script src="lib/js/html5shiv.js"></script>
		<![endif]-->
	</head>

	<body>

		<div class="reveal">

			<!-- Any section element inside of this container is displayed as a slide -->
			<div class="slides">
				<section>
					<h1>
						Latent Dirichlet Allocation <br />
						<span style="font-size:0.6em;">and</span> <br/>
						Conditional Random Fields
					</h1>
					<h2>in Text mining</h2>
					<p>Konrad Strack, Martyna Wałaszewska, Grzegorz Kaczmarczyk, Paweł Sikora</p>
				</section>

				<section>
					<section>
						<h2>Text mining</h2>
						<p>Process of deriving high-quality information from text</p>
						<ul>
							<li>Text categorization</li>
							<li>Text clustering</li>
							<li>Concept/entity extraction</li>
							<li>Sentiment analysis</li>
							<li>Document summarization</li>
							<li>Discovering topics</li>
						</ul>
					</section>
					<section>
						<h2>Analyzed documents</h2>
						<ul>
							<li>Press articles</li>
							<li>E-mails</li>
							<li>Open answers to survey questions</li>
							<li>Medical descriptions of symptoms</li>
							<li>CVs</li>
							<li>Social media and forums</li>
						</ul>
					</section>
				</section>

				<section>
					<section>
						<h2>Introduction to LDA</h2>
					</section>

					<section>
						<h2>History</h2>
						<ul>
							<li>tf–idf (1983)</li>
							<li>Latent semantic indexing (1988)</li>
							<li>Latent Dirichlet allocation (2003)</li>
							<li>Pachinko allocation model (2006)</li>
						</ul>
					</section>

					<section>
						<h2>Example</h2>

						<p>The <span class="highlight1">chair</span> is next to the <span class="highlight1">table</span>.</p>
						<p>There's some <span class="highlight2">cheese</span> on the <span class="highlight1">table</span>.</p>
						<p>Do you <span class="highlight3">want</span> some <span class="highlight2">wine</span>?</p>

						<br />
						<p>Topics:
							<ul>
								<li class="highlight1">furniture, architecture, ...</li>
								<li class="highlight2">food, dining, ...</li>
								<li class="highlight3">actions, feelings, ...</li>
							</ul>
						</p>
					</section>

					<section>
						<h3>Basic definitions</h3>
						<dl>
							<dt>word</dt>
							<dd>A basic unit of data</dd>

							<dt>document</dt>
							<dd>A sequence of words, denoted by $$\mathbf{w} = (w_1, w_2, \dotsc, w_N)$$</dd>

							<dt>corpus</dt>
							<dd>A collection of documents, denoted by	$$\mathcal{D} = \{\mathbf{w}_1, \mathbf{w}_2, \dotsc, \mathbf{w}_M\}$$</dd>
						</ul>
					</section>

					<section>
						<p>A generative process is assumed for each document $\mathbf{w}$ in the corpus:</p>

						<ul>
							<li>$N \sim Poisson(\xi)$</li>
							<li>$\Theta \sim Dir(\mathbf{\alpha})$</li>
							<li>For each word $w_n$:
								<ul>
									<li>We choose a topic $z_n \sim Multinomial(\Theta)$</li>
									<li>The word is chosen from a multinomial probability conditioned on the topic:
									$p(w_n | z_n, \mathbf{\beta})$</li>
								</ul>
							</li>
						</ul>
					</section>

					<section>
						<h3>Plate notation</h3>

						<img src="assets/images/Latent_Dirichlet_allocation.svg" alt="Plate notation of Latent Dirichlet Allocation" style="background: #eee; padding: 20px"/>
					</section>

					<section>
						<h2>Applications and extensions</h2>
						<ul>
							<li>Topic modeling</li>
							<li>Modeling relations between topics (distributions other than Dirichlet)</li>
							<li>Learning the hierarchy of topics (Hierarchical Dirichlet process)</li>
							<li>Categorization of images (Spatial LDA)</li>
						</ul>
					</section>
				</section>

				<section>
					<section>
						<h2>Conditional Random Fields</h2>
					</section>
					<section>
						<h2>History</h2>
						John Lafferty, Andrew McCallum, Fernando Pereira, "Conditional Random Fields: Probabilistic Models for Segmenting and Labeling Sequence Data." (2001)
					</section>
					<section>
						Let's define observations $X$ and random variables $Y$<br />
						$G=(V,E)$ graph such that $Y=(Y_v)$ $v \in V$<br /><br />	
						$(X, Y)$ is a Conditional random field when the random variables $Y_v$, conditioned on $X$, obey the Markov property with respect to the graph<br />
						$p(Y_v|X, Y_w, w \neq v) = p(Y_v|X, Y_w, w \sim v)$<br /><br />
						Markov property: property of a stochastic process. Conditional probability distribution of future states 
						depends only upon the present state
					</section>
					<section>
						<img src="assets/images/graph.png" style="background: #eee; padding: 20px"/><br /><br />
						$p_\theta(Y|X) = \frac{1}{Z_\theta(X)} exp \{\sum_{k=1}^{K}\theta_kF_k(X,Y)\} $<br />
						$F_k(X,Y) = \sum_{t=1}^T f_k(Y_{t-1}, Y_t, X_t)$
					</section>
					<section>
						Algorithm steps
						<ul>
							<li>Model training: learning the conditional distributions between the $Y_i$ and feature functions from some corpus of training data</li>
							<li>Inference: determining the probability of a given label sequence Y given X</li>
							<li>Decoding: determining the most likely label sequence Y given X</li>
						</ul>
					</section>
					<section>
						<h2>Applications</h2>
						<ul>
							<li>Text mining
							<ul>
								<li>Shallow parsing</li>
								<li>Entity extraction (aka Named entity recognition)</li>
								<li>Sentiment analysis</li>
							</ul></li>
							<li>Computational biology: Gene prediction</li>
							<li>Computer vision: Object recognition, Image segmentation</li>
							<li>Alternative to hiden Markov Models</li>
						</ul>
					</section>

				</section>

				<section>
					<section>
							<h2>LDA in R</h2>
					</section>
					
					<section>
						<p>Two LDA packages</p>
						<ul>
							<li>lda</li>
							<ul>
								<li>implementation written in R</li>
								<li>uses a collapsed Gibbs Sampler</li>
								<li>contains supervised LDA</li>
							</ul>
							<li>topicmodels</li>
							<ul>
								<li>implementation written in C by Blei et al.</li>
								<li>uses Variational EM and Gibbs Sampling</li>
								<li>contains Correlated Topics Models</li>
								<li>better documentation and input format</li>
							</ul>
						</ul>
					</section>
					
					<section>
						<h2>R: lda package</h2>
					</section>
					<section>
						<h3>Basic informations</h3>
						<ul>
							<li>Algorithms: LDA, sLDA, corrLDA</li>
							<li>Pre- and postprocessing tools</li>
						</ul>
					</section>
					<section>
						<h3>Related packages</h3>
						<p>There are a lot of packages supporting natural language
						processing, both lexical analysis and semantics analysis.
						They can be found in CRAN.</p>
						<ul>
							<li>tm - comprehensive text mining framework</li>
							<li>openNLP, RWeka - R interfaces to Java OpenNLP and Weka</li>
							<li>lsa</li>
							<li>topicmodels - interface to LDA C implementation</li>
						</ul>
					</section>
				</section>
				

				<section>
					<h2>Sources</h2>
					<ul>
						<li>Wikipedia:<ul>
							<li><a href="http://en.wikipedia.org/wiki/Text_mining">http://en.wikipedia.org/wiki/Text_mining</a> (13/10/2013)</li>
							<li><a href="http://en.wikipedia.org/wiki/Conditional_random_field">http://en.wikipedia.org/wiki/Conditional_random_field</a> (18/11/2013)</li>
						</ul>
						<li>Blei, David M., Andrew Y. Ng, and Michael I. Jordan. "Latent dirichlet allocation." the Journal of machine Learning research 3 (2003): 993-1022.</li>
						<li>Edward Chen, "Introduction to Latent Dirichlet Allocation": <a href="http://blog.echen.me/2011/08/22/introduction-to-latent-dirichlet-allocation/">http://blog.echen.me/2011/08/22/introduction-to-latent-dirichlet-allocation/</a> (18/11/2013)</li>
						<li>John Lafferty, Andrew McCallum, Fernando Pereira, "Conditional Random Fields: Probabilistic Models for Segmenting and Labeling Sequence Data." (2001)</li>
						<li>T. Lavergne, O. Cappé and F. Yvon "Practical very large scale CRFs" (2010)</li>
					</ul>

				</section>

			</div>

		</div>

		<script src="lib/js/head.min.js"></script>
		<script src="js/reveal.min.js"></script>

		<script>

			// Full list of configuration options available here:
			// https://github.com/hakimel/reveal.js#configuration
			Reveal.initialize({
				controls: true,
				progress: true,
				history: true,
				center: true,

				theme: Reveal.getQueryHash().theme, // available themes are in /css/theme
				transition: Reveal.getQueryHash().transition || 'default', // default/cube/page/concave/zoom/linear/fade/none

				// Optional libraries used to extend on reveal.js
				dependencies: [
					{ src: 'lib/js/classList.js', condition: function() { return !document.body.classList; } },
					{ src: 'plugin/markdown/marked.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
					{ src: 'plugin/markdown/markdown.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
					{ src: 'plugin/highlight/highlight.js', async: true, callback: function() { hljs.initHighlightingOnLoad(); } },
					{ src: 'plugin/zoom-js/zoom.js', async: true, condition: function() { return !!document.body.classList; } },
					{ src: 'plugin/notes/notes.js', async: true, condition: function() { return !!document.body.classList; } },
				  { src: 'plugin/math/math.js', async: true }
				],

				// mathjax
				math: {
			    mathjax: 'http://cdn.mathjax.org/mathjax/latest/MathJax.js',
					config: 'TeX-AMS_HTML-full'  // See http://docs.mathjax.org/en/latest/config-files.html
				}

			});

		</script>

	</body>
</html>
